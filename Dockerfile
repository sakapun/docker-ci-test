FROM golang:latest as builder

COPY src/* ./

RUN go build main.go


# start from scratch
FROM scratch

# copy our static linked executable
COPY --from=builder /go/main main

# tell how to run this container 
CMD ["./main"]